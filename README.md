colored_pointcloud package

Install: 
cd to the src folder of a ros workspace, git clone this package and catkin_make this workspace.

Preparation: 
calibrate your camera, and your lidar-camera system,
then write the intrinsic matrix, distortion coefficients and the extrinsic matrix to config/calib_result.yaml,
finally change the camera_topic and lidar_topic to fit your own system. 

Usage: 
<launch your camera and lidar nodes>
* launch with rviz
```
    roslaunch dynamic_obj_speed colored_pointcloud_node161.launch rviz:=1
```
* launch without rviz
```
    roslaunch dynamic_obj_speed colored_pointcloud_node161.launch rviz:=0
```

* play rosbag 
```
    cd path/to/bagfile/
    rosbag play xxx.bag
```
Docker build:
cd path/to/dynamicObjSpeed
docker build . -t dynamic_dwq
