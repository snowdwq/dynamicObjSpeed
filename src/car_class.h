#pragma once
#include <ros/ros.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <std_msgs/Header.h>
// #include "colored_pointcloud/colored_pointcloud.h"

class car_class
{
private:
    /* data */
public:
    car_class(/* args */);
    ~car_class();
    std_msgs::Header header; 
    std::string category;
    pcl::PointCloud<pcl::PointXYZRGB>  pointcloud; 
    pcl::PointXYZ center_of_mass;
};

car_class::car_class(/* args */)
{
}

car_class::~car_class()
{
}
