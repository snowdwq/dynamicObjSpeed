#include <ros/ros.h>
#include <boost/bind.hpp>
#include <Eigen/Dense>
#include <opencv2/opencv.hpp>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CompressedImage.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Header.h>
#include <dynamic_obj_speed/CarsSpeed.h>
#include <yolov5_ros/BoundingBoxes.h>
// #include <colored_pointcloud/BoundingBox.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/sync_policies/exact_time.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/image_encodings.h>
#include <cv_bridge/cv_bridge.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/common/transforms.h>	
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/pcd_io.h>
#include <pcl/common/common.h>
#include <pcl/common/centroid.h>
#include <math.h>
#include "colored_pointcloud/colored_pointcloud.h"
#include <sys/stat.h>
#include <sys/types.h> 
#include <cstdio>
#include <ctime>

// #include <tf2_ros/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Point.h>
#include <visualization_msgs/Marker.h>
#include <math.h>

#include "car_class.h"

#define YELLOW "\033[33m" /* Yellow */
#define GREEN "\033[32m"  /* Green */
#define REND "\033[0m" << std::endl

#define WARN (std::cout << YELLOW)
#define INFO (std::cout << GREEN)
#define PI 3.14159265


ros::Publisher fused_image_pub, colored_cloud_showpub;
ros::Publisher car_cloud_showpub;
ros::Publisher speed_pub;
ros::Publisher car_boundingBox_pub;
// colored_cloud_pub;

class RsCamFusion
{
  private:
    cv::Mat intrinsic;
    cv::Mat extrinsic;
    cv::Mat distcoeff;
    cv::Size imageSize;
    Eigen::Matrix4d transform, inv_transform;
    cv::Mat rVec = cv::Mat::zeros(3, 1, CV_64FC1); // Rotation vector
    cv::Mat rMat = cv::Mat::eye(3, 3, CV_64FC1);
    cv::Mat tVec = cv::Mat::zeros(3, 1, CV_64FC1); // Translation vector
    bool show_colored_cloud, save_data;
    std::string image_save_dir, cloud_save_dir, colored_cloud_save_dir;
    std::map<std::string, car_class> cur_car_map_, prev_car_map;

    int color[21][3] = 
    {
        {255, 0, 0}, {255, 69, 0}, {255, 99, 71}, 
        {255, 140, 0}, {255, 165, 0}, {238, 173, 14},
        {255, 193, 37}, {255, 255, 0}, {255, 236, 139},
        {202, 255, 112}, {0, 255, 0}, {84, 255, 159},
        {127, 255, 212}, {0, 229, 238}, {152, 245, 255},
        {178, 223, 238}, {126, 192, 238}, {28, 134, 238},
        {0, 0, 255}, {72, 118, 255}, {122, 103, 238} 
    };
    float color_distance;   //step length to color the lidar points according to plane distance(z)
    int frame_count = 0;

  public:
    RsCamFusion(cv::Mat cam_intrinsic, cv::Mat lidar2cam_extrinsic, cv::Mat cam_distcoeff, cv::Size img_size, float color_dis, bool show_cloud, bool save)
    {
      intrinsic = cam_intrinsic;
      extrinsic = lidar2cam_extrinsic;
      distcoeff = cam_distcoeff;
      transform(0,0) = extrinsic.at<double>(0,0);
      transform(0,1) = extrinsic.at<double>(0,1);
      transform(0,2) = extrinsic.at<double>(0,2);
      transform(0,3) = extrinsic.at<double>(0,3);
      transform(1,0) = extrinsic.at<double>(1,0);
      transform(1,1) = extrinsic.at<double>(1,1);
      transform(1,2) = extrinsic.at<double>(1,2);
      transform(1,3) = extrinsic.at<double>(1,3);
      transform(2,0) = extrinsic.at<double>(2,0);
      transform(2,1) = extrinsic.at<double>(2,1);
      transform(2,2) = extrinsic.at<double>(2,2);
      transform(2,3) = extrinsic.at<double>(2,3);
      transform(3,0) = extrinsic.at<double>(3,0);
      transform(3,1) = extrinsic.at<double>(3,1);
      transform(3,2) = extrinsic.at<double>(3,2);
      transform(3,3) = extrinsic.at<double>(3,3);
      inv_transform = transform.inverse();
      imageSize = img_size;
      color_distance = color_dis;
      show_colored_cloud = show_cloud;
      save_data = save;
      if(save_data)
      {
        time_t rawtime;
        struct tm *ptminfo;
        time(&rawtime);
        ptminfo = localtime(&rawtime);
        std::string currentdate = "/data/" + std::to_string(ptminfo->tm_year + 1900) + std::to_string(ptminfo->tm_mon + 1) 
                                          + std::to_string(ptminfo->tm_mday) + std::to_string(ptminfo->tm_hour) 
                                          + std::to_string(ptminfo->tm_min) + std::to_string(ptminfo->tm_sec);
        mkdir(currentdate.c_str(),S_IRUSR | S_IWUSR | S_IXUSR | S_IRWXG | S_IRWXO);
        image_save_dir = currentdate + "/front_camera";
        mkdir(image_save_dir.c_str(),S_IRUSR | S_IWUSR | S_IXUSR | S_IRWXG | S_IRWXO);
        cloud_save_dir = currentdate + "/rslidar_points";
        mkdir(cloud_save_dir.c_str(),S_IRUSR | S_IWUSR | S_IXUSR | S_IRWXG | S_IRWXO);
        colored_cloud_save_dir = currentdate + "/colored_cloud";
        mkdir(colored_cloud_save_dir.c_str(),S_IRUSR | S_IWUSR | S_IXUSR | S_IRWXG | S_IRWXO);
      }
    }

    void callback(const sensor_msgs::ImageConstPtr input_image_msg,
                const sensor_msgs::PointCloud2ConstPtr input_cloud_msg,
                const yolov5_ros::BoundingBoxesConstPtr car_msg) //
    {
      cv::Mat input_image;
      cv::Mat undistorted_image;
      cv_bridge::CvImagePtr cv_ptr; 

      std_msgs::Header image_header = input_image_msg->header;
      std_msgs::Header cloud_header = input_cloud_msg->header;
      // INFO << image_header << REND;

      auto car_detection = car_msg; 
    // sensor_msgs to cv image  
      std::cout << "\n \n" ;
      ROS_INFO("bounding_boxes size: %li, input_cloud_msg size: %li", car_detection->bounding_boxes.size(), input_cloud_msg->data.size());
      if(car_detection->bounding_boxes.size() > 0 && input_cloud_msg->data.size() >0){
        try
        {
          cv_ptr = cv_bridge::toCvCopy(input_image_msg, sensor_msgs::image_encodings::BGR8);
        }
        catch(cv_bridge::Exception e)
        {
          ROS_ERROR_STREAM("Cv_bridge Exception:"<<e.what());
          return;
        }
        input_image = cv_ptr->image;
        
        //sensor_msgs to pointxyzi
        pcl::PointCloud<pcl::PointXYZI>::Ptr input_cloud_ptr(new pcl::PointCloud<pcl::PointXYZI>);
        pcl::PointCloud<pcl::PointXYZI>::Ptr transformed_cloud (new pcl::PointCloud<pcl::PointXYZI>);
        pcl::fromROSMsg(*input_cloud_msg, *input_cloud_ptr);
        if (input_cloud_ptr->size() == 0)
        {
          WARN << "input cloud is empty, please check it out!" << REND;
        }

        //transform lidar points from lidar coordinate to camera coordiante
        pcl::transformPointCloud (*input_cloud_ptr, *transformed_cloud, transform);        //lidar coordinate(forward x+, left y+, up z+) 
                                                                                          //camera coordiante(right x+, down y+, forward z+) (3D-3D)  
                                                                                          //using the extrinsic matrix between this two coordinate system
        std::vector<cv::Point3d> lidar_points;
        std::vector<cv::Scalar> dis_color;
        std::vector<float> intensity;
        std::vector<cv::Point2d> imagePoints;
        
        //reserve the points in front of the camera(z>0)
        for(long unsigned int i=0;i<=transformed_cloud->points.size();i++)
        {
            if(transformed_cloud->points[i].z > 0)
            {
              lidar_points.push_back(cv::Point3d(transformed_cloud->points[i].x, transformed_cloud->points[i].y, transformed_cloud->points[i].z));
              int color_order = int(transformed_cloud->points[i].z / color_distance);
              if(color_order > 20)
              {
                color_order = 20;
              }
              dis_color.push_back(cv::Scalar(color[color_order][2], color[color_order][1], color[color_order][0]));
              intensity.push_back(transformed_cloud->points[i].intensity);
            }
        }

        //project lidar points from the camera coordinate to the image coordinate(right x+, down y+)
        cv::projectPoints(lidar_points, rMat, tVec, intrinsic, distcoeff, imagePoints);          
        
        pcl::PointCloud<PointXYZRGBI>::Ptr colored_cloud (new pcl::PointCloud<PointXYZRGBI>);
        pcl::PointCloud<PointXYZRGBI>::Ptr colored_cloud_transback (new pcl::PointCloud<PointXYZRGBI>);
        cv::Mat image_to_show = input_image.clone();
	ROS_INFO("processing image points");
        for(long unsigned int i=0;i < imagePoints.size();i++)
        {
          if(imagePoints[i].x>=0 && imagePoints[i].x<800 && imagePoints[i].y>=0 && imagePoints[i].y<600)
          {
	    //ROS_INFO("image points size, x: ");
            cv::circle(image_to_show, imagePoints[i], 1, dis_color[i], 2, 8, 0);
            PointXYZRGBI point;                                                             //reserve the lidar points in the range of image 
            point.x = lidar_points[i].x;                                                        //use 3D lidar points and RGB value of the corresponding pixels  
            point.y = lidar_points[i].y;                                                        //to create colored point clouds
            point.z = lidar_points[i].z;
            point.r = input_image.at<cv::Vec3b>(imagePoints[i].y, imagePoints[i].x)[2];
            point.g = input_image.at<cv::Vec3b>(imagePoints[i].y, imagePoints[i].x)[1];
            point.b = input_image.at<cv::Vec3b>(imagePoints[i].y, imagePoints[i].x)[0];
            point.i = intensity[i];
            colored_cloud->points.push_back(point);  
          }
        }
	ROS_INFO("colored cloud finished");
        //transform colored points from camera coordinate to lidar coordinate
        pcl::transformPointCloud (*colored_cloud, *colored_cloud_transback, inv_transform);      
        
        if(show_colored_cloud)
        {  
          pcl::PointCloud<pcl::PointXYZRGB>::Ptr colored_cloud_toshow (new pcl::PointCloud<pcl::PointXYZRGB>);
          for(long unsigned int i=0;i<colored_cloud_transback->points.size();i++)
          {
              pcl::PointXYZRGB point;                                                             
              point.x = colored_cloud_transback->points[i].x;                                                        
              point.y = colored_cloud_transback->points[i].y;                                                        
              point.z = colored_cloud_transback->points[i].z;
              point.r = colored_cloud_transback->points[i].r;
              point.g = colored_cloud_transback->points[i].g;
              point.b = colored_cloud_transback->points[i].b;
              colored_cloud_toshow->points.push_back (point);  
            }
          ROS_INFO("publishing colored cloud");
          publishCloudtoShow(colored_cloud_showpub, cloud_header, colored_cloud_toshow);
        }
        ROS_INFO("color point finished");
        bool calcu_car_points = true; 
        if(calcu_car_points && car_detection->bounding_boxes.size() > 0) {

          pcl::PointCloud<pcl::PointXYZRGB> car_points_to_show;
          cur_car_map_.clear();// clear exist data
          for(long unsigned int i =0; i< car_detection->bounding_boxes.size(); i++){
            ROS_INFO("calcu_car_points: car class: %s", car_detection->bounding_boxes[i].Class.c_str());
            // std::cout << car_detection->bounding_boxes[i].Class << std::endl; 
            auto boundingBox = car_detection->bounding_boxes[i];
            std::string carCate = boundingBox.Class;
            // std::cout << boundingBox.Class <<": ";
            cur_car_map_[carCate].category = carCate;
            cur_car_map_[carCate].header = car_detection->header;
            cur_car_map_[carCate].header.frame_id = "ego_vehicle/lidar/lidar1";
            // std::cout << car_detection->header.stamp.toSec() << std::endl;
            int64 xmin = boundingBox.xmin;
            int64 ymin = boundingBox.ymin; 
            int64 xmax = boundingBox.xmax; 
            int64 ymax = boundingBox.ymax;
            // plot rectangle for bounding box to image_to_show
            // cv::Rect rect(xmin, ymin, xmax, ymax);
            // cv::Rect rect(20, 20, 90, 90);
            cv::Point pt1(xmin, ymin);
            cv::Point pt2(xmax, ymax);
            cv::rectangle(image_to_show, pt1, pt2, cv::Scalar(0, 255, 0), 5);
            // get car points
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointcloudCam_tmp (new pcl::PointCloud<pcl::PointXYZRGB>);
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointcloudlidar_tmp (new pcl::PointCloud<pcl::PointXYZRGB>);
            // pcl::PointCloud<pcl::PointXYZRGB> pointcloudCam_tmp; 
            // pcl::PointCloud<pcl::PointXYZRGB> pointcloudlidar_tmp; 
            for(long unsigned int i=0;i < imagePoints.size();i++)
            {
              if(imagePoints[i].x>= xmin+2 && imagePoints[i].x<= xmax-2 && imagePoints[i].y>=ymin+2 && imagePoints[i].y<= ymax-2)
              {
                // cv::circle(image_to_show, imagePoints[i], 1, dis_color[i], 2, 8, 0);
                pcl::PointXYZRGB point;                                                             //reserve the lidar points in the range of image 
                point.x = lidar_points[i].x;                                                        //use 3D lidar points and RGB value of the corresponding pixels  
                point.y = lidar_points[i].y;                                                        //to create colored point clouds
                point.z = lidar_points[i].z;
                point.r = input_image.at<cv::Vec3b>(imagePoints[i].y, imagePoints[i].x)[2];
                point.g = input_image.at<cv::Vec3b>(imagePoints[i].y, imagePoints[i].x)[1];
                point.b = input_image.at<cv::Vec3b>(imagePoints[i].y, imagePoints[i].x)[0];
                // point.i = intensity[i];
                pointcloudCam_tmp->push_back(point);
                
              }
            }
            //transform colored points from camera coordinate to lidar coordinate
            pcl::transformPointCloud (*pointcloudCam_tmp, *pointcloudlidar_tmp, inv_transform); 
            car_points_to_show += *pointcloudlidar_tmp;
            cur_car_map_[carCate].pointcloud = *pointcloudlidar_tmp;
            //calculate center of mass of a car
            if(pointcloudlidar_tmp->size() > 0){
              Eigen::Vector4f centroid;			
              pcl::compute3DCentroid(*pointcloudlidar_tmp, centroid);	
              cur_car_map_[carCate].center_of_mass.x = centroid[0];
              cur_car_map_[carCate].center_of_mass.y = centroid[1];
              cur_car_map_[carCate].center_of_mass.z = centroid[2];
              std::cout << "Class name: " << carCate << "\ncenter of mass: " << cur_car_map_[carCate].center_of_mass << std::endl;
            }
              

            std::cout <<" points size: "<< cur_car_map_[carCate].pointcloud.size() << std::endl;
            // publishCloudtoShow(car_cloud_showpub, cur_car_map_[carCate].header, pointcloudlidar_tmp);
            

          }
          std::cout << "Points in total:" << car_points_to_show.size() << std::endl; 
          // std::cout << "\n\n\n";
          pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointshow_tmp (new pcl::PointCloud<pcl::PointXYZRGB>);
          *pointshow_tmp = car_points_to_show;
          auto header_tmp = car_detection->header;
          header_tmp.frame_id = "ego_vehicle/lidar/lidar1";
          if(car_points_to_show.size() > 0) publishCloudtoShow(car_cloud_showpub, header_tmp, pointshow_tmp);
          

          // estimate the speed of cars
          bool calcu_speed = true; 
          dynamic_obj_speed::CarSpeed carSpeedMsg;
          dynamic_obj_speed::CarsSpeed CarsSpeedMsg;
          // parameters for linelist
          visualization_msgs::Marker line_list; 
          line_list.header = car_detection->header; 
          line_list.header.frame_id = "ego_vehicle/lidar/lidar1";
          line_list.ns = "colored_pointcloud_node";
          line_list.action = visualization_msgs::Marker::ADD;
          line_list.pose.orientation.w = 1.0;
          line_list.id = 0;
          line_list.type = visualization_msgs::Marker::LINE_LIST;
          line_list.scale.x = 0.1;
          line_list.color.g = 1.0;
          line_list.color.a = 1.0;          

          if(calcu_speed){
            for (auto it = cur_car_map_.begin(); it != cur_car_map_.end(); ++it){
              //draw 3d bounding box for car
              auto carPointcloud_bb = it->second.pointcloud;
              if(carPointcloud_bb.size() > 0){              
                pcl::PointXYZRGB minPt, maxPt;
                pcl::getMinMax3D(carPointcloud_bb, minPt, maxPt);
                float xmin, ymin, zmin;
                float xmax, ymax, zmax; 
                xmin = minPt.x;
                ymin = minPt.y;
                zmin = minPt.z;

                xmax = maxPt.x;
                ymax = maxPt.y;
                zmax = maxPt.z;

                std::vector<geometry_msgs::Point> p;
                geometry_msgs::Point p1;
                // 0
                p1.x = xmin; p1.y = ymin, p1.z = zmin;
                p.push_back(p1);

                // 1
                p1.x = xmin; p1.y = ymin, p1.z = zmax;
                p.push_back(p1);

                // 2
                p1.x = xmin; p1.y = ymax, p1.z = zmax;
                p.push_back(p1);

                // 3
                p1.x = xmin; p1.y = ymax, p1.z = zmin;
                p.push_back(p1);

                // 4
                p1.x = xmax; p1.y = ymin, p1.z = zmin;
                p.push_back(p1);

                // 5
                p1.x = xmax; p1.y = ymin, p1.z = zmax;
                p.push_back(p1);

                // 6
                p1.x = xmax; p1.y = ymax, p1.z = zmax;
                p.push_back(p1);

                // 7
                p1.x = xmax; p1.y = ymax, p1.z = zmin;
                p.push_back(p1);

                for(size_t i=0; i<3; i++ ){
                  line_list.points.push_back(p[i]);
                  line_list.points.push_back(p[i+1]);                  
                }
                line_list.points.push_back(p[3]);
                line_list.points.push_back(p[0]);

                for(size_t i=4; i<7; i++ ){
                  line_list.points.push_back(p[i]);
                  line_list.points.push_back(p[i+1]);                  
                }
                line_list.points.push_back(p[7]);
                line_list.points.push_back(p[4]);

                for(size_t i=0; i<4; i++ ){
                  line_list.points.push_back(p[i]);
                  line_list.points.push_back(p[i+4]);                  
                }
              } //loop for each car,  end of car bounding box
              car_boundingBox_pub.publish(line_list);











              // calculate the highest height 
              std::string cur_car = it->first;              
              auto prev_car = prev_car_map.find(cur_car);
              if(prev_car != prev_car_map.end()){
                // std::cout  <<"\n Car class: "<< cur_car <<std::endl; 
                // calculate central point
                auto cur_carClass = it->second;              
                auto prev_carClass = prev_car->second;

                auto cur_carPoints = cur_carClass.pointcloud;
                auto prev_carPoints = prev_carClass.pointcloud;
                
                double speed = 0;
                double pos_x= 0, pos_y=0, pos_z=0;

                pcl::PointXYZRGB car_hpoint, prev_hpoint; 
                car_hpoint.z = -1000;
                prev_hpoint.z = -1000; 

                // if(cur_carPoints.size()>0 || prev_carPoints.size() > 0){
                  
                //get the highest point
                if(cur_carPoints.size()>0){
                  //comment out the highest height
                  // for(size_t i=0; i< cur_carPoints.size(); i++){
                  //   if(cur_carPoints[i].z > car_hpoint.z){
                  //     car_hpoint = cur_carPoints[i];
                  //   }
                  // }

                  // replace highest position with center of mass
                  car_hpoint.x = cur_carClass.center_of_mass.x;
                  car_hpoint.y = cur_carClass.center_of_mass.y;
                  car_hpoint.z = cur_carClass.center_of_mass.z;
                  
                  pos_x = car_hpoint.x;
                  pos_y = car_hpoint.y;
                  pos_z = car_hpoint.z;
                }

                if(prev_carPoints.size() > 0){
                  //comment out the highest height
                  // for(size_t i=0; i< prev_carPoints.size(); i++){
                  //   if(prev_carPoints[i].z > prev_hpoint.z){
                  //     prev_hpoint = prev_carPoints[i];
                  //   }
                  // }
                  // replace highest position with center of mass
                  prev_hpoint.x = prev_carClass.center_of_mass.x; 
                  prev_hpoint.y = prev_carClass.center_of_mass.y; 
                  prev_hpoint.z = prev_carClass.center_of_mass.z;
                  
                }
                double distance = 0;
                double time_tmp = 0;
                double angle = 0;

                if(cur_carPoints.size()>0 && prev_carPoints.size() > 0){
                  distance = std::sqrt(std::pow((car_hpoint.x-prev_hpoint.x), 2) + std::pow((car_hpoint.y-prev_hpoint.y), 2));
                  time_tmp = cur_carClass.header.stamp.toSec()  - prev_carClass.header.stamp.toSec();
                  
                  if(time_tmp > 0 && distance> 0.1) speed = distance/time_tmp; 

                  //calculate angle:
                  double dy = car_hpoint.y - prev_hpoint.y;
                  double dx = car_hpoint.x - prev_hpoint.x; 

                  if(dx == 0 && dy == 0) angle = 0;
                  else{
                    angle = atan2(dy, dx)*180/PI;
                  }
                  if(angle < 0) angle = angle + 360;// (0, 360), y direction is 0;


                  
                }
                if(cur_carPoints.size()>0){      
                  ROS_INFO("car class: %s,  distance:%f, time:%f, speed: %f", cur_carClass.category.c_str(), distance, time_tmp, speed);           
                  carSpeedMsg.Class = cur_carClass.category;
                  carSpeedMsg.speed = speed;
                  carSpeedMsg.x = pos_x; 
                  carSpeedMsg.y = pos_y; 
                  carSpeedMsg.z = angle; // replace Z with angle.  Keep attention on this!!!!!!
                  
                  // if(pos_x < 17 && pos_x > 0 && speed > 5){
                  //   std::cout << "\n****************** Height detect **************************************\n";
                  //   ROS_INFO("Position of Car, x: %lf m, y: %lf m, Car height: 1.43m,", pos_x, pos_y);
                  //   ROS_INFO("Height limit bar position, x: 15 m, y: -8.4 m, bar height 3 m"); 
                  //   ROS_INFO("Relative height: 1.57m");
                  //   std::cout << "*********************** End *****************************************\n";
                  // } 


                  
                  CarsSpeedMsg.CarSpeed.push_back(carSpeedMsg);
                }


                    
                  




                // }
                


              }
              
            }
            
            // std::cout <<"\n";
          }







          prev_car_map = cur_car_map_; 
          CarsSpeedMsg.header = car_detection->header;
          speed_pub.publish(CarsSpeedMsg);
          publishImage(fused_image_pub, image_header, image_to_show);
        }


        
        if(save_data)
        {
          saveData(image_header, input_image, cloud_header, input_cloud_ptr, colored_cloud_transback);
        }
        frame_count = frame_count + 1;
      }
    }

    void publishImage(const ros::Publisher& image_pub, const std_msgs::Header& header, const cv::Mat image)
    {
      cv_bridge::CvImage output_image;
      output_image.header.frame_id = header.frame_id;
      output_image.encoding = sensor_msgs::image_encodings::TYPE_8UC3;
      output_image.image = image;
      image_pub.publish(output_image);
    } 
    
    void saveData(const std_msgs::Header& image_header, const cv::Mat image, const std_msgs::Header& cloud_header,
                      const pcl::PointCloud<pcl::PointXYZI>::ConstPtr& cloud, pcl::PointCloud<PointXYZRGBI>::Ptr colored_cloud)
    {
      //timestamp: image_header.stamp.sec . image_header.stamp.nsec 
      std::string img_name = std::to_string(frame_count) + "_" + std::to_string(image_header.stamp.sec) + "_" + std::to_string(image_header.stamp.nsec) + ".jpg";
      std::string cloud_name = std::to_string(frame_count) + "_" + std::to_string(cloud_header.stamp.sec) + "_" + std::to_string(cloud_header.stamp.nsec) + ".pcd";
      std::string colored_cloud_name = "c_" + std::to_string(frame_count) + "_" + std::to_string(cloud_header.stamp.sec) + "_" + std::to_string(cloud_header.stamp.nsec) + ".pcd";
      cv::imwrite(image_save_dir + "/" + img_name, image);
      pcl::io::savePCDFileASCII(cloud_save_dir + "/" + cloud_name, *cloud);
      colored_cloud->width = cloud->size();
      colored_cloud->height = 1;
      colored_cloud->is_dense = false;
      colored_cloud->points.resize(cloud->width * cloud->height);
      pcl::io::savePCDFileASCII(colored_cloud_save_dir + "/" + colored_cloud_name, *colored_cloud);
    }

    void publishCloudtoShow(const ros::Publisher& cloudtoshow_pub, const std_msgs::Header& header,
                      const pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr& cloud)
    {
      sensor_msgs::PointCloud2 output_msg;
      pcl::toROSMsg(*cloud, output_msg);
      output_msg.header = header;
      cloudtoshow_pub.publish(output_msg);
    }
};

int main(int argc, char** argv)
{
  // yolov5_ros::BoundingBoxes test_msg; 
  ros::init(argc, argv, "colored_pointcloud_node");
  ros::NodeHandle nh;
  ros::NodeHandle priv_nh("~");

  std::string config_path, file_name;
  std::string camera_topic, lidar_topic;
  std::string car_topic;

  float color_dis;
  bool show_cloud, save_data;
  if (priv_nh.hasParam("calib_file_path") && priv_nh.hasParam("file_name"))
  {
    priv_nh.getParam("camera_topic", camera_topic);
    priv_nh.getParam("lidar_topic", lidar_topic);
    priv_nh.getParam("car_topic", car_topic);
    priv_nh.getParam("calib_file_path", config_path);
    priv_nh.getParam("file_name", file_name);
    priv_nh.getParam("color_distance", color_dis);
    priv_nh.getParam("show_colored_cloud", show_cloud);
    priv_nh.getParam("save_data", save_data);
  }
  else
  {
    WARN << "Config file is empty!" << REND;
    return 0;
  }
  
  INFO << "config path: " << config_path << REND;
  INFO << "config file: " << file_name << REND;

  std::string config_file_name = config_path + "/" + file_name;
  cv::FileStorage fs_reader(config_file_name, cv::FileStorage::READ);
  
  cv::Mat cam_intrinsic, lidar2cam_extrinsic, cam_distcoeff;
  cv::Size img_size;
  fs_reader["CameraMat"] >> cam_intrinsic;
  fs_reader["CameraExtrinsicMat"] >> lidar2cam_extrinsic;
  fs_reader["DistCoeff"] >> cam_distcoeff;
  fs_reader["ImageSize"] >> img_size;
  fs_reader.release();

  if (lidar_topic.empty() || camera_topic.empty())
  {
    WARN << "sensor topic is empty!" << REND;
    return 0;
  }

  INFO << "lidar topic: " << lidar_topic << REND;
  INFO << "camera topic: " << camera_topic << REND;
  INFO << "camera intrinsic matrix: " << cam_intrinsic << REND;
  INFO << "lidar2cam entrinsic matrix: " << lidar2cam_extrinsic << REND;
  
  RsCamFusion fusion(cam_intrinsic, lidar2cam_extrinsic, cam_distcoeff, img_size, color_dis, show_cloud, save_data); 
  message_filters::Subscriber<sensor_msgs::Image> camera_sub(nh, camera_topic, 30);
  message_filters::Subscriber<sensor_msgs::PointCloud2> lidar_sub(nh, lidar_topic, 10);
  message_filters::Subscriber<yolov5_ros::BoundingBoxes> carCategories_sub(nh, car_topic, 10);
  // message_filters::Subscriber<dynamic_obj_speed::BoundingBoxes> carCategories_sub; //dynamic_obj_speed::BoundingBoxes 
  // carCategories_sub.subscribe(nh, car_topic, 10);
  
  typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Image, sensor_msgs::PointCloud2, yolov5_ros::BoundingBoxes> MySyncPolicy;
	// typedef message_filters::Synchronizer<MySyncPolicy> Synchronizer;

  message_filters::Synchronizer<MySyncPolicy> sync(MySyncPolicy(200), camera_sub, lidar_sub, carCategories_sub);
  sync.registerCallback(boost::bind(&RsCamFusion::callback, &fusion, _1, _2, _3));


  fused_image_pub = nh.advertise<sensor_msgs::Image>("fused_image", 10);
  colored_cloud_showpub = nh.advertise<sensor_msgs::PointCloud2>("colored_cloud_toshow", 10);
  car_cloud_showpub = nh.advertise<sensor_msgs::PointCloud2>("car_cloud_toshow", 10);
  speed_pub = nh.advertise<dynamic_obj_speed::CarsSpeed>("car_speed_and_position", 10);
  car_boundingBox_pub = nh.advertise<visualization_msgs::Marker>("car_boundingBox", 10);
  
  ros::spin();
  return 0;

}


