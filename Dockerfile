FROM ros:noetic-ros-base

LABEL Maintainer="Wenqiang Du <snowdwq@gmail.com>"
LABEL Dynamic3D_project.version="0.1"

# RUN sed -i'' 's/archive\.ubuntu\.com/ca\.archive\.ubuntu\.com/' /etc/apt/sources.list &&\
#     apt-get clean && \
#     apt-get autoclean && \
#     rm -rf /var/lib/apt/lists/* && \
RUN apt-get update 

# RUN apt-get install  -y --fix-missing --no-install-recommends 
RUN apt-get install -y  --fix-missing --no-install-recommends git ros-noetic-cv-bridge ros-noetic-image-transport ros-noetic-pcl-ros

# RUN apt-get install  ros-noetic-tf2 
    
RUN mkdir -p /dynamic3d_ws/src &&\
    cd /dynamic3d_ws/src &&\     
    git clone https://gitlab.com/snowdwq/yolov5_ros.git &&\
    git clone https://gitlab.com/snowdwq/dynamicObjSpeed.git

WORKDIR /dynamic3d_ws

RUN . /opt/ros/${ROS_DISTRO}/setup.sh  &&\
    catkin_make &&\
    echo "source /dynamic3d_ws/devel/setup.bash"  >> ~/.bashrc

